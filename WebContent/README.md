## 框架与基本功能
- [x] 主页(index.html)
- [x] 登陆(login.html)
- [x] 注册(register.html)
- [x] 信息(infomation.html)
- [x] 网站介绍(introduction.html)
- [x] 管理(admin.html)

## 需要进一步完善的功能(按重要性排序)

- [x] 网站安全性，不能通过修改url的方式进入无权限的页面(例如手动修改url为management.html，绕过登陆界面)
- [x] 文件下载功能
- [x] management  导航栏/删除文章/查看用户等功能
- [ ] management  CSS
- [ ] weather.html 增加实时天气功能
- [x] information  CSS/评论显示美化 
- [x] information  CSS/评论提交美化
- [ ] introduction  CSS/网站介绍
- [ ] 其他页面美化

## 可选的新功能
- [ ] 主页下拉菜单，列出所有国家，点击进入
- [ ] 验证码/邮箱验证功能

## 2019/5/30 v1.1

* 完成登陆页面基本框架(login.html)
* 完成注册页面基本框架(register.html)

login.html form提交接口

    <form action="login.jsp" method="POST">

        <input id="login-name" class="login-box-inner" name="login-name" type="text" placeholder="用户名"/>

        <input id="login-password"  class="login-box-inner" name="login-password" type="password" placeholder="密码"/>

        <input id="login-button" class="login-box-inner" type="submit" name="login" value="登录"/>

    </form>

register.html form提交接口

	<form action="register.jsp" method="POST">

		<input id="register-name" class="register-box-inner" name="register-name" type="text" placeholder="用户名"/>

		<input id="register-password"  class="register-box-inner" name="register-password" type="password" placeholder="密码"/>

		<input id="confirm-password"  class="register-box-inner" name="confirm-password" type="password" placeholder="确认密码"/>

		<input id="register-button" class="register-box-inner" type="submit" name="register" value="注册"/>
		
	</form>

## 2019/6/2 v1.3

* 完成主页页面基本框架(index.html)
* 完成信息页面基本框架(infomation.html)

## 2019/6/2 v1.4

* 完成网站介绍页面基本框架(introduction.html)

## 2019/6/7 v1.5

* 完成网站登录/注销页面基本框架与功能
	* login.html
	* regester.html
	* login.jsp
	* logout.jsp
	
## 2019/6/7 v1.6

* 小bug修复
	
## 2019/6/10 v1.7

* 增加管理页面基本框架与删除用户功能(management.html/.jsp/.js)
* 增加评论显示/发布/文件上传功能(information.html/.jsp/.js;addComment.jsp/getComment.jsp)
* 完成数据库搭建

国家名称数据详见contries.txt 用于生成数据库/下拉菜单

## 2019/6/10 v1.7
* 修复上传文件时文件名中文乱码