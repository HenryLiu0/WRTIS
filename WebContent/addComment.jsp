<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<%@ page import="java.io.*,java.util.*,org.apache.commons.io.*"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="org.apache.commons.fileupload.disk.*"%>
<%@ page import="org.apache.commons.fileupload.servlet.*"%>
<% request.setCharacterEncoding("utf-8"); //设定接受的字符串的编码为utf-8 %>

<%! 
 	boolean add_comment(String text, String userName, String countryName, 
 			String file, String img1, String img2, String img3){
		
		String msg = "";
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String conStr = "jdbc:mysql://172.18.187.10:3306/WRTIS16337097"
					+ "?autoReconnect=true&useSSL=true&useUnicode=true&characterEncoding=UTF-8";
					Connection con =DriverManager.getConnection(conStr,"user", "123");
			Statement stmt =con.createStatement();
			String sql = String.format("insert into comment(commentText,userName,countryName,commentFile,commentImage1,commentImage2,commentImage3)"+
							"value('%s','%s','%s','%s','%s','%s','%s')",text,userName,countryName,file,img1,img2,img3);
			stmt.executeUpdate(sql);
			stmt.close();
			con.close();
			msg = "添加comment完毕"+":"+userName+' '+countryName+"\n";
			System.out.print(msg);
			return true;
		}
		catch(Exception e){
			msg = e.getMessage();
			System.out.print("连接失败\n");
			System.out.println(msg);
			return false;
		}
	}
	
	//计算存放的子目录  
	private String makeChildDirectory(String realPath, String fileName) {  
	      
	    String directory = ""+File.separator + fileName;  
	    File file = new File(realPath,directory);  
	    if(!file.exists())  
	        file.mkdirs();  
	      
	    return directory;  
}  


%>


<%
	String countryName = request.getParameter("countryName");
	String userName = (String)session.getAttribute("user");
	
	if(userName == null || userName.equals("null")){
		out.print("未登录，请登录后再操作");
		response.sendRedirect("login.html");
	}else{
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);//是否用multipart提交的?
		if(isMultipart) {
			FileItemFactory factory = new DiskFileItemFactory();
			//factory.setSizeThreshold(yourMaxMemorySize); //设置使用的内存最大值
			
			//factory.setRepository(tmpFile);
			
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(6*1024*1024); //允许的最大文件尺寸
			List items = upload.parseRequest(request);

			String[] dir;
			dir = new String[5];
			
			for(int i = 0; i < items.size(); i++) {
				FileItem fi = (FileItem) items.get(i);
				if(!fi.isFormField()){//如果是文件
					DiskFileItem dfi = (DiskFileItem) fi;
					if(!dfi.getName().trim().equals("")) {//getName()返回文件名称或空串
						
						Random rand = new Random();

						// 使用相对路径标记需要下载及保存到sql中的文件，而不是绝对路径
						String sqlFileName = "files"
						+ System.getProperty("file.separator")
						+ rand.nextInt(999999999)
						+ FilenameUtils.getName(dfi.getName());
						
						String fileName = application.getRealPath(".")
						+ System.getProperty("file.separator")
						+ sqlFileName;
						
		                // File file = new File("/file");
						// if(!file.exists()){  
						//     file.mkdirs();  
						// }   
					
						dir[i] = (String)new File(sqlFileName).getPath().replaceAll("\\\\", "\\\\\\\\");
						dir[i] = new String(dir[i].getBytes("utf-8"),"utf-8");
						System.out.print(new File(fileName).getAbsolutePath() + "\n");
						dfi.write(new File(fileName));
					}
					else{
						dir[i] = "";
					}
				}
				//文字
				else{
					dir[i] = fi.getString("utf-8");
				}
			}
			boolean flag = add_comment(dir[0], userName, countryName, dir[1], dir[2], dir[3], dir[4]);
		}
	}
	
	String url = request.getHeader("Referer");
	response.sendRedirect(url);
%>