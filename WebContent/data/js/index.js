window.onload = function(){
	setUserName();
	initURL();
	setLink("navTitle", "introduction.html");
	var map = document.createElement("script");
	map.src = "data/js/worldMap.js";
	map.defer = "defer";

//  生成地图
	  setCountries(countriesFullName);
	  document.body.appendChild(map);
}

//国家名字
var countriesFullName = {
	"countries": [
		"Afghanistan",
		"AFRICA",
		"Albania",
		"Algeria",
		"Angola",
		"Antigua and Barbuda",
		"Arab Rep of Egypt",
		"Argentina",
		"Armenia",
		"Aruba",
		"ASIA",
		"Australia",
		"Austria",
		"Azerbaijan",
		"The Bahamas",
		"Bahrain",
		"Bangladesh",
		"Barbados",
		"Belarus",
		"Belgium",
		"Belize",
		"Benin",
		"Bhutan",
		"Bolivia",
		"Bosnia and Herzegovina",
		"Botswana",
		"Brazil",
		"Brunei Darussalam",
		"Bulgaria",
		"Burkina Faso",
		"Burundi",
		"Cote-d-Ivoire",
		"Cabo Verde",
		"Cambodia",
		"Cameroon",
		"Canada",
		"Caribbean",
		"Central African Republic",
		"Central America",
		"Central Asia",
		"Chad",
		"Channel Islands",
		"Chile",
		"China",
		"Hong Kong SAR-China",
		"Macao SAR China",
		"Colombia",
		"Comoros",
		"Congo",
		"Costa Rica",
		"Croatia",
		"Cuba",
		"Curacao",
		"Cyprus",
		"Czech Republic",
		"Dem Peoples Rep of Korea",
		"Dem Rep of Congo",
		"Denmark",
		"Djibouti",
		"Dominican Republic",
		"Eastern Africa",
		"Eastern Asia",
		"Eastern Europe",
		"Ecuador",
		"El Salvador",
		"Equatorial Guinea",
		"Eritrea",
		"Estonia",
		"Ethiopia",
		"EUROPE",
		"Federated States of Micronesia",
		"Fiji",
		"Finland",
		"France",
		"French Guiana",
		"French Polynesia",
		"FYR Macedonia",
		"Gabon",
		"The Gambia",
		"Georgia",
		"Germany",
		"Ghana",
		"Greece",
		"Grenada",
		"Guadeloupe",
		"Guam",
		"Guatemala",
		"Guinea",
		"Guinea-Bissau",
		"Guyana",
		"Haiti",
		"Honduras",
		"Hungary",
		"Iceland",
		"India",
		"Indonesia",
		"Islamic Republic of Iran",
		"Iraq",
		"Ireland",
		"Israel",
		"Italy",
		"Jamaica",
		"Japan",
		"Jordan",
		"Kazakhstan",
		"Kenya",
		"Kiribati",
		"Kuwait",
		"Kyrgyz Republic",
		"Lao PDR",
		"LATIN AMERICA AND THE CARIBBEAN",
		"Latvia",
		"Lebanon",
		"Lesotho",
		"Liberia",
		"Libya",
		"Lithuania",
		"Luxembourg",
		"Madagascar",
		"Malawi",
		"Malaysia",
		"Maldives",
		"Mali",
		"Malta",
		"Martinique",
		"Mauritania",
		"Mauritius",
		"Mayotte",
		"Melanesia",
		"Mexico",
		"Micronesia",
		"Middle Africa",
		"Moldova",
		"Mongolia",
		"Montenegro",
		"Morocco",
		"Mozambique",
		"Myanmar",
		"Namibia",
		"Nepal",
		"The Netherlands",
		"New Caledonia",
		"New Zealand",
		"Nicaragua",
		"Niger",
		"Nigeria",
		"Northern Africa",
		"NORTHERN AMERICA",
		"Northern Europe",
		"Norway",
		"OCEANIA",
		"Oman",
		"Pakistan",
		"Panama",
		"Papua New Guinea",
		"Paraguay",
		"Peru",
		"Philippines",
		"Poland",
		"Polynesia",
		"Portugal",
		"Puerto Rico",
		"Qatar",
		"Reunion",
		"RB-de-Venezuela",
		"Rep of Korea",
		"Rep of Yemen",
		"Romania",
		"Russian Federation",
		"Rwanda",
		"St-Lucia",
		"St-Vincent and the Grenadines",
		"Samoa",
		"Sao Tome and Principe",
		"Saudi Arabia",
		"Senegal",
		"Serbia",
		"Seychelles",
		"Sierra Leone",
		"Singapore",
		"Slovak Republic",
		"Slovenia",
		"Solomon Islands",
		"Somalia",
		"South Africa",
		"South America",
		"South Sudan",
		"South-Central Asia",
		"South-Eastern Asia",
		"Southern Africa",
		"Southern Asia",
		"Southern Europe",
		"Spain",
		"Sri Lanka",
		"West Bank and Gaza",
		"Sub-Saharan Africa",
		"Sudan",
		"Suriname",
		"Swaziland",
		"Sweden",
		"Switzerland",
		"Syrian Arab Rep",
		"Tajikistan",
		"Tanzania",
		"Thailand",
		"Timor-Leste",
		"Togo",
		"Tonga",
		"Trinidad and Tobago",
		"Tunisia",
		"Turkey",
		"Turkmenistan",
		"Uganda",
		"Ukraine",
		"United Arab Emirates",
		"United Kingdom",
		"United States",
		"US Virgin Islands",
		"Uruguay",
		"Uzbekistan",
		"Vanuatu",
		"Vietnam",
		"Western Africa",
		"Western Asia",
		"Western Europe",
		"Zambia",
		"Zimbabwe"
		]
}

var myDate = new Date();
var nowDate = myDate.getFullYear() + "-" + (myDate.getMonth()+1) + "-" + myDate.getDate();

function sleep(numberMillis) {
    var now = new Date();
    var exitTime = now.getTime() + numberMillis;
    while (true) {
        now = new Date();
        if (now.getTime() > exitTime)
            return;
    }
}


function setUserName(){
   $.ajax({
		url:"login.jsp",
		type:"POST",
		dataType:"text",
		async: true,
		data:{
			type:"getUserName"
		},
		success: function(userName){
			userName = userName.trim();
			if(userName != "" && userName != null && userName != "null"){
				var loginRegister = document.getElementById("loginRegister");
				loginRegister.innerHTML='<a class="user" href="logged.html">' + userName + '</a>';
	            loginRegister.innerHTML+='&nbsp &nbsp <a class="user" href="logout.jsp">' + '注销' + '</a>';
			}else{
				console.log("Not logged yet");
			}
			
		},
		error: function(){
			console.log("Check failed");
		}
    })
}


function setLink(name, url){
	var button = document.getElementById(name);
	button.style.cursor = "pointer";
	button.onclick = function(){
		window.open(url);
	}
}

function setCountries(countries){
    countries = countries["countries"];
    for(var i=0; i<countries.length; i++)
    {
        getPopulation(countries[i]);
    }
}

function getPopulation(name){
    // console.log("http://54.72.28.201:80/1.0/population/" + name  + "/" + nowDate + "/?format=json");
    $.ajax({
        type:"GET",     
		url:"http://54.72.28.201:80/1.0/population/" + name  + "/" + nowDate + "/?format=json",
		dataType: "json",   
		// timeout: 1500,  
        success: function(data){
			setPopulation(name, data, true);
        },
        error :function(){
			console.log(name + " 人口数据加载超时");
        }
    })
}

function setPopulation(name, data, flag)
{
    if(flag)
    {
        var countryName = name.toLowerCase();
        try{
            var population = data["total_population"]["population"];
        }
        catch(error){
            console.log("error name " + name);
        }

        var index = findAbbr(countriesName, countryName);
        if(index != false)
        {
            // simplemaps_worldmap_mapdata["state_specific"][index]["description"] = population;
            // console.log(simplemaps_worldmap_mapdata["state_specific"][index]);
            try{
                simplemaps_worldmap_mapdata["state_specific"][index]["description"] = population;
            }
            catch(error){
                console.log("index no in Mapdataset " + index);
            }
        }
    }
}

//国家名字
var countriesName = {BD:"Bangladesh",BE:"Belgium",BF:"Burkina Faso",BG:"Bulgaria",BA:"Bosnia and Herzegovina",BB:"Barbados",BL:"Saint-Barthélemy",BM:"Bermuda",BN:"Brunei",BO:"Bolivia",JP:"Japan",BI:"Burundi",BJ:"Benin",BT:"Bhutan",JM:"Jamaica",BW:"Botswana",BR:"Brazil",BS:"Bahamas",BY:"Belarus",BZ:"Belize",TN:"Tunisia",RW:"Rwanda",RS:"Serbia",TL:"Timor-Leste",RE:"Reunion (France)",TM:"Turkmenistan",TJ:"Tajikistan",RO:"Romania",GW:"Guinea-Bissau",GT:"Guatemala",GR:"Greece",GQ:"Equatorial Guinea",GP:"Guadeloupe (France)",BH:"Bahrain",GY:"Guyana",GF:"France",GE:"Georgia",GD:"Grenada",GB:"United Kingdom",GA:"Gabon",GN:"Guinea",GM:"The Gambia",GL:"Greenland",GH:"Ghana",OM:"Oman",JO:"Jordan",HR:"Croatia",HT:"Haiti",HU:"Hungary",HN:"Honduras",PR:"Puerto Rico",PS:"Palestine",PW:"Palau",PT:"Portugal",KN:"Saint Kitts and Nevis",PY:"Paraguay",AI:"Anguilla",PA:"Panama",PF:"French Polynesia",PG:"Papua New Guinea",PE:"Peru",PK:"Pakistan",PH:"Philippines",PL:"Poland",ZM:"Zambia",BQSE:"St. Eustatius (Netherlands)",EH:"Western Sahara",RU:"Russian Federation",EE:"Estonia",EG:"Egypt",ZA:"South Africa",EC:"Ecuador",IT:"Italy",VN:"Vietnam",SB:"Solomon Islands",ET:"Ethiopia",SO:"Somalia",ZW:"Zimbabwe",KY:"Cayman Islands",ES:"Spain",ER:"Eritrea",ME:"Montenegro",MD:"Moldova",MG:"Madagascar",MF:"Saint Martin (French)",MA:"Morocco",UZ:"Uzbekistan",MM:"Myanmar",ML:"Mali",MN:"Mongolia",MH:"Marshall Islands",MK:"Macedonia",MU:"Mauritius",MT:"Malta",MW:"Malawi",MV:"Maldives",MQ:"Martinique (France)",MP:"Northern Mariana Islands",MS:"Montserrat",MR:"Mauritania",UG:"Uganda",MY:"Malaysia",MX:"Mexico",IL:"Israel",FR:"France",FI:"Finland",FJ:"Fiji",FK:"Falkland Islands",FO:"Faeroe Islands",NI:"Nicaragua",NL:"Netherlands",NO:"Norway",NA:"Namibia",VU:"Vanuatu",NC:"New Caledonia",NE:"Niger",NG:"Nigeria",NZ:"New Zealand",BQBO:"Netherlands",NP:"Nepal",NR:"Nauru",XK:"Kosovo",CI:"Côte d'Ivoire",CH:"Switzerland",CO:"Colombia",CN:"China",CM:"Cameroon",CL:"Chile",CA:"Canada",CG:"Republic of Congo",CF:"Central African Republic",CD:"Democratic Republic of the Congo",CZ:"Czech Republic",CY:"Cyprus",CR:"Costa Rica",CW:"Curaco (Netherlands)",CV:"Cape Verde",CU:"Cuba",SZ:"Swaziland",SY:"Syria",SX:"Saint Martin (Dutch)",KG:"Kyrgyzstan",KE:"Kenya",SS:"South Sudan",SR:"Suriname",KH:"Cambodia",SV:"El Salvador",KM:"Comoros",ST:"São Tomé and Principe",SK:"Slovakia",KR:"South Korea",SI:"Slovenia",KP:"North Korea",KW:"Kuwait",SN:"Senegal",SL:"Sierra Leone",SC:"Seychelles",KZ:"Kazakhstan",SA:"Saudi Arabia",SG:"Singapore",SE:"Sweden",SD:"Sudan",DO:"Dominican Republic",DM:"Dominica",DJ:"Djibouti",DK:"Denmark",VG:"British Virgin Islands",DE:"Germany",YE:"Yemen",DZ:"Algeria",US:"United States",UY:"Uruguay",YT:"Mayotte (France)",LB:"Lebanon",LC:"Saint Lucia",LA:"Laos",TV:"Tuvalu",TW:"Taiwan",TT:"Trinidad and Tobago",TR:"Turkey",LK:"Sri Lanka",LV:"Latvia",TO:"Tonga",LT:"Lithuania",LU:"Luxembourg",LR:"Liberia",LS:"Lesotho",TH:"Thailand",TG:"Togo",TD:"Chad",TC:"Turks and Caicos Islands",BQSA:"Saba (Netherlands)",LY:"Libya",VC:"Saint Vincent and the Grenadines",AE:"United Arab Emirates",VE:"Venezuela",AG:"Antigua and Barbuda",AF:"Afghanistan",IQ:"Iraq",VI:"United States Virgin Islands",IS:"Iceland",IR:"Iran",AM:"Armenia",AL:"Albania",AO:"Angola",AS:"American Samoa",AR:"Argentina",AU:"Australia",AT:"Austria",AW:"Aruba",IN:"India",TZ:"Tanzania",IC:"Canary Islands (Spain)",AZ:"Azerbaijan",IE:"Ireland",ID:"Indonesia",UA:"Ukraine",QA:"Qatar",MZ:"Mozambique"};

function findAbbr(dict, value) 
{ 
    for(var key in dict)
    {
        if(countriesName[key].toLowerCase() == value)
            return key;
    }
    return false;
}

//初始化url
function initURL()
{
    for(var key in simplemaps_worldmap_mapdata["state_specific"])
    {
        simplemaps_worldmap_mapdata["state_specific"][key]["url"] = "information.html?abbr=" + key + "&name=" + countriesName[key];
    }
}

//数据部分
var simplemaps_worldmap_mapdata = {
	main_settings:{
		//General settings
		width: 'responsive', //or 'responsive'
		background_color: '#FFFFFF',	
		background_transparent: 'yes',
		border_color: '#ffffff',
		popups: 'detect', //on_click, on_hover, or detect
	
		//State defaults
		state_description: 'Sorry, no data',
		state_color: '#88A4BC',
		state_hover_color: '#3B729F',
		state_url: '',
		border_size: 1.5,		
		all_states_inactive: 'no',
		all_states_zoomable: 'no',		
		
		//Location defaults
		location_description: 'Sorry, no data',
		location_color: '#FF0067',
		location_opacity: .8,
		location_hover_opacity: 1,
		location_url: '',
		location_size: 25,
		location_type: 'square', // circle, square, image
		location_image_source: 'frog.png', //name of image in the map_images folder		
		location_border_color: '#FFFFFF',
		location_border: 2,
		location_hover_border: 2.5,				
		all_locations_inactive: 'no',
		all_locations_hidden: 'no',
		
		//Labels
		label_color: '#d5ddec',	
		label_hover_color: '#d5ddec',		
		label_size: 25,
		label_font: 'Arial',
		hide_labels: 'no',
		
		//Zoom settings
		zoom: 'yes', //use default regions
		back_image: 'no', //Use image instead of arrow for back zoom				
		initial_back: 'no', //Show back button when zoomed out and do this JavaScript upon click		
		initial_zoom: -1,  //-1 is zoomed out, 0 is for the first continent etc	
		initial_zoom_solo: 'no', //hide adjacent states when starting map zoomed in
		region_opacity: 1,
		region_hover_opacity: .6,
		zoom_out_incrementally: 'yes',  // if no, map will zoom all the way out on click
		zoom_percentage: .99,
		zoom_time: .5, //time to zoom between regions in seconds
		
		//Popup settings
		popup_color: 'white',
		popup_opacity: .9,
		popup_shadow: 1,
		popup_corners: 5,
		popup_font: '18px/1.5 Verdana, Arial, Helvetica, sans-serif',
		popup_nocss: 'no', //use your own css	
		
		//Advanced settings
		div: 'map',
		auto_load: 'yes',		
		url_new_tab: 'yes', 
		images_directory: 'default', //e.g. 'map_images/'
		fade_time:  0.3, //time to fade out		
		link_text: 'View news'  //Text mobile browsers will see for links	
	},
	state_specific: {
		"AF":{
			name: "Afghanistan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AO":{
			name: "Angola",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AL":{
			name: "Albania",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AE":{
			name: "United Arab Emirates",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AR":{
			name: "Argentina",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AM":{
			name: "Armenia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AU":{
			name: "Australia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AT":{
			name: "Austria",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AZ":{
			name: "Azerbaijan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BI":{
			name: "Burundi",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BE":{
			name: "Belgium",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BJ":{
			name: "Benin",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BF":{
			name: "Burkina Faso",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BD":{
			name: "Bangladesh",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BG":{
			name: "Bulgaria",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BH":{
			name: "Bahrain",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},    
		"BA":{
			name: "Bosnia and Herzegovina",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BY":{
			name: "Belarus",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BZ":{
			name: "Belize",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BO":{
			name: "Bolivia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BR":{
			name: "Brazil",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BN":{
			name: "Brunei Darussalam",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BT":{
			name: "Bhutan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BW":{
			name: "Botswana",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CF":{
			name: "Central African Republic",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CA":{
			name: "Canada",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CH":{
			name: "Switzerland",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CL":{
			name: "Chile",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CN":{
			name: "China",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CI":{
			name: "Côte d'Ivoire",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CM":{
			name: "Cameroon",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CD":{
			name: "Democratic Republic of the Congo",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CG":{
			name: "Republic of Congo",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CO":{
			name: "Colombia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CR":{
			name: "Costa Rica",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CU":{
			name: "Cuba",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CZ":{
			name: "Czech Republic",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"DE":{
			name: "Germany",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"DJ":{
			name: "Djibouti",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"DK":{
			name: "Denmark",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"DO":{
			name: "Dominican Republic",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"DZ":{
			name: "Algeria",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"EC":{
			name: "Ecuador",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"EG":{
			name: "Egypt",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ER":{
			name: "Eritrea",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"EE":{
			name: "Estonia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ET":{
			name: "Ethiopia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"FI":{
			name: "Finland",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"FJ":{
			name: "Fiji",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GA":{
			name: "Gabon",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GB":{
			name: "United Kingdom",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GE":{
			name: "Georgia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GH":{
			name: "Ghana",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GN":{
			name: "Guinea",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GM":{
			name: "The Gambia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GW":{
			name: "Guinea-Bissau",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GQ":{
			name: "Equatorial Guinea",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GR":{
			name: "Greece",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GL":{
			name: "Greenland",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GT":{
			name: "Guatemala",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GY":{
			name: "Guyana",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"HN":{
			name: "Honduras",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"HR":{
			name: "Croatia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"HT":{
			name: "Haiti",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"HU":{
			name: "Hungary",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ID":{
			name: "Indonesia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"IN":{
			name: "India",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"IE":{
			name: "Ireland",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"IR":{
			name: "Iran",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"IQ":{
			name: "Iraq",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"IS":{
			name: "Iceland",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"IL":{
			name: "Israel",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"IT":{
			name: "Italy",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"JM":{
			name: "Jamaica",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"JO":{
			name: "Jordan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"JP":{
			name: "Japan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KZ":{
			name: "Kazakhstan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KE":{
			name: "Kenya",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KG":{
			name: "Kyrgyzstan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KH":{
			name: "Cambodia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KR":{
			name: "Republic of Korea",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"XK":{
			name: "Kosovo",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KW":{
			name: "Kuwait",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LA":{
			name: "Lao PDR",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LB":{
			name: "Lebanon",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LR":{
			name: "Liberia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LY":{
			name: "Libya",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LK":{
			name: "Sri Lanka",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LS":{
			name: "Lesotho",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LT":{
			name: "Lithuania",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LU":{
			name: "Luxembourg",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LV":{
			name: "Latvia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MA":{
			name: "Morocco",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MD":{
			name: "Moldova",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MG":{
			name: "Madagascar",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MX":{
			name: "Mexico",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MK":{
			name: "Macedonia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ML":{
			name: "Mali",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MM":{
			name: "Myanmar",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ME":{
			name: "Montenegro",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MN":{
			name: "Mongolia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MZ":{
			name: "Mozambique",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MR":{
			name: "Mauritania",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MW":{
			name: "Malawi",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MY":{
			name: "Malaysia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NA":{
			name: "Namibia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NE":{
			name: "Niger",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NG":{
			name: "Nigeria",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NI":{
			name: "Nicaragua",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NL":{
			name: "Netherlands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NO":{
			name: "Norway",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NP":{
			name: "Nepal",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NZ":{
			name: "New Zealand",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"OM":{
			name: "Oman",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PK":{
			name: "Pakistan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PA":{
			name: "Panama",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PE":{
			name: "Peru",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PH":{
			name: "Philippines",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PG":{
			name: "Papua New Guinea",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PL":{
			name: "Poland",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KP":{
			name: "Dem. Rep. Korea",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PT":{
			name: "Portugal",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PY":{
			name: "Paraguay",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PS":{
			name: "Palestine",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"QA":{
			name: "Qatar",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"RO":{
			name: "Romania",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"RU":{
			name: "Russia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"RW":{
			name: "Rwanda",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"EH":{
			name: "Western Sahara",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SA":{
			name: "Saudi Arabia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SD":{
			name: "Sudan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SS":{
			name: "South Sudan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SN":{
			name: "Senegal",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SL":{
			name: "Sierra Leone",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SV":{
			name: "El Salvador",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"RS":{
			name: "Serbia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SR":{
			name: "Suriname",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SK":{
			name: "Slovakia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SI":{
			name: "Slovenia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SE":{
			name: "Sweden",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SZ":{
			name: "Swaziland",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SY":{
			name: "Syria",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TD":{
			name: "Chad",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TG":{
			name: "Togo",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TH":{
			name: "Thailand",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TJ":{
			name: "Tajikistan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TM":{
			name: "Turkmenistan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TL":{
			name: "Timor-Leste",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TN":{
			name: "Tunisia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TR":{
			name: "Turkey",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TW":{
			name: "Taiwan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TZ":{
			name: "Tanzania",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"UG":{
			name: "Uganda",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"UA":{
			name: "Ukraine",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"UY":{
			name: "Uruguay",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"US":{
			name: "United States",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"UZ":{
			name: "Uzbekistan",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"VE":{
			name: "Venezuela",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"VN":{
			name: "Vietnam",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"VU":{
			name: "Vanuatu",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"YE":{
			name: "Yemen",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ZA":{
			name: "South Africa",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ZM":{
			name: "Zambia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ZW":{
			name: "Zimbabwe",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SO":{
			name: "Somalia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GF":{
			name: "France",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"FR":{
			name: "France",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ES":{
			name: "Spain",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AW":{
			name: "Aruba",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AI":{
			name: "Anguilla",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AD":{
			name: "Andorra",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"AG":{
			name: "Antigua and Barbuda",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BS":{
			name: "Bahamas",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BM":{
			name: "Bermuda",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"BB":{
			name: "Barbados",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KM":{
			name: "Comoros",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CV":{
			name: "Cape Verde",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KY":{
			name: "Cayman Islands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"DM":{
			name: "Dominica",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"FK":{
			name: "Falkland Islands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"FO":{
			name: "Faeroe Islands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GD":{
			name: "Grenada",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"HK":{
			name: "Hong Kong",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"KN":{
			name: "Saint Kitts and Nevis",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LC":{
			name: "Saint Lucia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"LI":{
			name: "Liechtenstein",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MF":{
			name: "Saint Martin (French)",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MV":{
			name: "Maldives",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MT":{
			name: "Malta",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MS":{
			name: "Montserrat",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MU":{
			name: "Mauritius",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NC":{
			name: "New Caledonia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"NR":{
			name: "Nauru",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PN":{
			name: "Pitcairn Islands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PR":{
			name: "Puerto Rico",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"PF":{
			name: "French Polynesia",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SG":{
			name: "Singapore",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SB":{
			name: "Solomon Islands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"ST":{
			name: "São Tomé and Principe",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SX":{
			name: "Saint Martin (Dutch)",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"SC":{
			name: "Seychelles",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TC":{
			name: "Turks and Caicos Islands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TO":{
			name: "Tonga",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"TT":{
			name: "Trinidad and Tobago",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"VC":{
			name: "Saint Vincent and the Grenadines",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"VG":{
			name: "British Virgin Islands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"VI":{
			name: "United States Virgin Islands",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CY":{
			name: "Cyprus",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"RE":{
			name: "Reunion (France)",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"YT":{
			name: "Mayotte (France)",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"MQ":{
			name: "Martinique (France)",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"GP":{
			name: "Guadeloupe (France)",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"CW":{
			name: "Curaco (Netherlands)",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		},
		"IC":{
			name: "Canary Islands (Spain)",
			description: "default",
			color: "default",
			hover_color: "default",
			url: "default"
		}
	},
	locations:{ 
	}
};
