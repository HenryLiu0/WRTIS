window.onload = function(){
    setLink("navTitle", "introduction.html");
    names = setTitle("browserTitle");
    setTitle("mainTitle");
    setUserName();
    getNews(names.name);
    setCommentCountry(names.name);
    setComment(names.name);
}

function setUserName(){
	   $.ajax({
			url:"login.jsp",
			type:"POST",
			dataType:"text",
			async: true,
			data:{
				type:"getUserName"
			},
			success: function(userName){
				userName = userName.trim();
				if(userName != "" && userName != null && userName != "null"){
					var loginRegister = document.getElementById("loginRegister");
					loginRegister.innerHTML='<a class="user" href="logged.html">' + userName + '</a>';
		            loginRegister.innerHTML+='&nbsp &nbsp <a class="user" href="logout.jsp">' + '注销' + '</a>';
				}else{
					console.log("Not logged yet");
				}
				
			},
			error: function(){
				console.log("Check failed");
			}
	    })
	}

function setLink(name, url){
	var button = document.getElementById(name);
	button.style.cursor = "pointer";
	button.onclick = function(){
		window.open(url);
	}
}

function getParameter(name){
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r!=null)
    return unescape(r[2]); 
    return null;
}

function setTitle(id){
    var abbr = getParameter("abbr");
    var name = getParameter("name");
    var title = document.getElementById(id);
    title.innerHTML = "News of " + name;
    return {abbr:abbr, name: name};
}

function getNews(name){
    nowDate = new Date();
    nowDate.setDate(nowDate.getDate()-3);
    var year = nowDate.getFullYear();
    var month = nowDate.getMonth()+1;
    var day = nowDate.getDate();
    var date = year + "-" + month + "-" + day;

    var Request = new XMLHttpRequest();
    console.log("https://newsapi.org/v2/everything?q=" + name + "&from=" + date + "&apiKey=147228df5a5d4b74b56eed4b771272f8");
    Request.open("GET", "https://newsapi.org/v2/everything?q=" + name + "&from=" + date + "&apiKey=147228df5a5d4b74b56eed4b771272f8", true);
    Request.send(null);
    Request.onreadystatechange = function(){
        if (Request.readyState == 4 && Request.status == 200) {
            //转换成JSON
            var data = eval("("+Request.responseText+")");
            setNews(data);
        }     
    }
}

function setNews(data){
    var articles = data.articles;
    console.log(data);
    console.log(articles);
    for(var info in articles)
        document.getElementById("articles").append(makeArticle(articles[info]));
    var loading = document.getElementById("loading");
    loading.style.display = "none";
}

function makeArticle(data)
{
    var article = document.createElement("div");
    article.className = "article";

    var title = document.createElement("h3");
    title.className = "title";
    title.innerHTML = data.title;
    title.onclick = function(){
        window.open(data.url);
    }

    var date = document.createElement("p");
    date.className = "date";
    date.innerHTML = data.publishedAt; 

    var author = document.createElement("p");
    author.className = "author";
    author.innerHTML = data.author;

    var content = document.createElement("p");
    content.className = "content";
    content.innerHTML = data.content;

    var img = document.createElement("img");
    img.className = "img";
    img.src = data.url;

    article.appendChild(title);
    article.appendChild(date);
    article.appendChild(author);
    // article.appendChild(img);
    article.appendChild(content);

    return article;
}

function setCommentCountry(name)
{
	var form = document.getElementById("add-coment-form");
	form.action = "addComment.jsp?countryName=" + name;

}

function setComment(name){
	$.ajax({
		url:"getComment.jsp",
		type:"POST",
		data:{"countryName": name},
		datatype:"text",
		success:function(data){
            var jsonData = eval("("+data+")");
            var commentArray = jsonData.commentArray;
            console.log(jsonData);
            console.log(commentArray);
			// var comments = document.getElementById("comments-content");
			// comments.innerHTML = data;
            // console.log(data);
            makeComments(commentArray);
		},
		error:function(){
			console.log("评论请求错误");
		}
	})
}

function makeComments(commentArray) {
    // console.log(data);
    for(var info in commentArray) {
        var data = commentArray[info];
        console.log(data);
        var a_content = document.createElement("div");
        a_content.className = "a-content";

            var comment_header = document.createElement("div");
            comment_header.className = "comment-header";
                var p0 = document.createElement("p");
                var span0 = document.createElement("span");
                span0.innerHTML = data.userName;
                var span1 = document.createElement("span");
                span1.innerHTML = " 说：";
                p0.appendChild(span0);
                p0.appendChild(span1);
            comment_header.appendChild(p0);

            var comment_content = document.createElement("div");
            comment_content.className = "comment-content";
                var p1 = document.createElement("p");
                p1.innerHTML = data.commentText;
            if(data.commentText != "")
                comment_content.appendChild(p1);

            var comment_img = document.createElement("div");
            comment_img.className = "comment-img";
                var p2 = document.createElement("p");
                    var img1 = document.createElement("img");
                    img1.setAttribute("src", data.img1);
                    var img2 = document.createElement("img");
                    img2.setAttribute("src", data.img2);
                    var img3 = document.createElement("img");
                    img3.setAttribute("src", data.img3);
                p2.appendChild(img1);
                p2.appendChild(img2);
                p2.appendChild(img3);
            if(!(data.img1 == "" && data.img2 == "" && data.img3 == ""))
                comment_img.appendChild(p2);

            var comment_file = document.createElement("div");
            comment_file.className = "content-file";
                var p3 = document.createElement("p");
                    var a0 = document.createElement("a");
                    a0.setAttribute("href", data.commentFile);
                    a0.innerHTML = data.commentFile.substring(data.commentFile.indexOf("\\")+1);
                p3.appendChild(a0);
            if(data.commentFile != "")
                comment_file.appendChild(p3);
        a_content.appendChild(comment_header);
        a_content.appendChild(comment_content);
        a_content.appendChild(comment_img);
        a_content.appendChild(comment_file);

        document.getElementById("comments-content").appendChild(a_content);
    }

}