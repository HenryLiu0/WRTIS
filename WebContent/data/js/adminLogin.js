window.onload = function(){
    alignItem("content");

}

window.onresize = function(){
    alignItem("content");
}

function alignItem(id)
{
    var item = document.getElementById(id);
    var windowHeight= document.documentElement.clientHeight;
    var margin_top=(windowHeight-item.offsetHeight)/2;
    item.style.marginTop = margin_top + "px";
}

//动态检查输入
function changeCheck(){
    var name = document.getElementById("login-name").value.trim();
    var pass = document.getElementById("login-password").value.trim();

    var flag = true;
    if(name == null || name=="")
        flag = false;
        
    if(pass == null || pass=="")
        flag = false;

    if(flag)
    {
        var button = document.getElementById("login-button");
        button.style.background = "rgba(0, 209, 0, 0.5)";
        button.removeAttribute("disabled");
        button.style.cursor = "pointer";
    }
    else{
        var button = document.getElementById("login-button");
        button.style.background = "";
        button.setAttribute("disabled", "true");
        button.style.cursor = "not-allowed";
    }
    return flag;
}

function loginButton(){
	var formData = {};
	$.map($("#login-form").serializeArray(), function(n, i){
		formData[n['name']] = n['value'];
	})
	formData["type"] = "adminLogin";
	$.ajax({
		url:"adminLogin.jsp",
		type:"POST",
		data: formData,
		dataType:"text",
		success:function(data){
			data=data.trim();
			if(data=="success")
			{
				alert("登陆成功");
				window.location.replace("management.jsp");
			}
			else if(data=="fail")
			{
				alert("密码错误");
				window.location.replace("adminLogin.html");
			}
			else if(data=="logged")
			{
				alert("已登录,请先注销当前帐号");
				window.location.replace("management.jsp");
			}
		},
		error:function(){
			console.log("登陆验证失败(可能是服务器错误)");
		}
	})
}
