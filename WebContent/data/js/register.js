window.onload = function(){
    alignItem("content");
}

window.onresize = function(){
    alignItem("content");
}

//动态检查输入
function changeCheck(){
    var name = document.getElementById("register-name").value.trim();
    var pass1 = document.getElementById("register-password").value.trim();
    var pass2 = document.getElementById("confirm-password").value.trim();
    var checkLogo = document.getElementById("checkLogo");
    var msg = document.getElementById("msg");

    var flag = true;
    msg.innerHTML = "";
    if(name == null || name=="")
    {
        msg.innerHTML += "请输入用户名 ";
        flag = false;
    }
        
    if(pass1 == null || pass1=="")
    {
        msg.innerHTML += "请输入密码 ";
        flag = false;
    }
    else if(pass2 == null || pass2 == "" || pass1 != pass2)
    {
        msg.innerHTML += "两次输入的密码不一致 ";
        flag = false;
    }
        
    checkLogo.className = flag?"fa fa-check":"fa fa-times";
    if(flag)
    {
        var button = document.getElementById("register-button");
        button.style.background = "rgba(0, 209, 0, 0.5)";
        button.removeAttribute("disabled");
        button.style.cursor = "pointer";
    }
    else{
        var button = document.getElementById("register-button");
        button.style.background = "";
        button.setAttribute("disabled", "true");
        button.style.cursor = "not-allowed";
    }
    return flag;
}

function alignItem(id)
{
    var item = document.getElementById(id);
    var windowHeight= document.documentElement.clientHeight;
    var margin_top=(windowHeight-item.offsetHeight)/2;
    item.style.marginTop = margin_top + "px";
}

function registerButton(){
	var formData={}
	$.map($("#register-form").serializeArray(), function(n, i){
		formData[n['name']] = n['value'];
	})
	formData["type"] = "register";
	
	console.log(formData);
	
	$.ajax({
		url:"register.jsp",
		type:"POST",
		data:formData,
		dataType: "text",
		success:function(data){
			data=data.trim();
			if(data=="existed"){
				alert("用户名已存在");
				window.location.replace("register.html");
			}
			else if(data=="fail"){
				alert("服务器连接错误");
				window.location.replace("register.html");
			}
			else if(data=="success"){
				alert("注册成功");
				window.location.replace("index.html");
			}
		},
		error:function(){
			console.log("注册失败");
		}
	})
}
