function delUser(){
	var formData = {};
	$.map($("#del-user-form").serializeArray(), function(n, i){
		formData[n['name']] = n['value'];
	})
	formData["type"] = "del-user";
	
	$.ajax({
		url:"management.jsp",
		type:"POST",
		data: formData,
		dataType:"text",
		success:function(data){
			data=data.trim();
			if(data=="success")
			{
				alert("删除成功");
			}
			else if(data=="fail")
			{
				alert("删除失败");
			}
		},
		error:function(){
			console.log("可能是服务器错误");
		}
	})
}
