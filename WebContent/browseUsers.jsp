<%@ page language="java" import="java.util.*,java.sql.*" 
         contentType="text/html; charset=utf-8"
%>
<%!
ArrayList< HashMap<String, Object> > query_all_user(){
    String msg;
    ArrayList< HashMap<String, Object> > list = new ArrayList< HashMap<String, Object> >();
    
    try{
        Class.forName("com.mysql.jdbc.Driver");
        String conStr = "jdbc:mysql://172.18.187.10:3306/WRTIS16337097"
        + "?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8";
        Connection con =DriverManager.getConnection(conStr,"user", "123");
        Statement stmt =con.createStatement();
        ResultSet rs=stmt.executeQuery("select * from user");
        while(rs.next()){
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("userId", rs.getInt("userId"));
            map.put("userName", rs.getString("userName"));
            map.put("userPassword", rs.getString("userPassword"));
            list.add(map);
        }
        rs.close();
        stmt.close();
        con.close();
        msg = "查询所有user完毕";
        System.out.println(msg);
        return list;
    }
    catch(Exception e){
        msg = e.getMessage();
        System.out.println("连接失败\n");
        System.out.println(msg);
        return null;
    }
}

%>
<% 
	request.setCharacterEncoding("utf-8"); //设定接受的字符串的编码为utf-8 

	String adminLoginStatus = (String)session.getAttribute("adminLogin");
	if("ok".equals(adminLoginStatus)) {
		System.out.println("登录情况下通过session访问management.jsp");
        String adminName = (String)session.getAttribute("admin");
        StringBuilder user_msg = new StringBuilder();
        ArrayList< HashMap<String, Object> > list = query_all_user();
        user_msg.append("<table><tr><th>id</th><th>姓名</th><th>密码</th><th>-</th><th>-</th></tr>");
        for(int i=0;i<list.size();i++){
            HashMap<String, Object> map = list.get(i);
            user_msg.append(
                String.format("<tr><td>%d</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>",
                    map.get("userId"),
                    map.get("userName"),
                    map.get("userPassword"),
                    String.format("<a href='browseComments.jsp?&userName=%s'  target='_blank'>查看评论</a>",
                        map.get("userName")
                    ),
                    String.format("<a href='deleteUser.jsp?userId=%d&userName=%s'>删除</a>",
                        map.get("userId"),
                        map.get("userName")
                    )
                )
            );
        }
        user_msg.append("</table>");
%>

<!DOCTYPE HTML>
<html>
<head>
<title>浏览学生名单</title>
<style>
	table{
		border-collapse: collapse;
		border: none;
		width: 500px;
	}
	td,th{
		border: solid grey 1px;            
		margin: 0 0 0 0;
		padding: 5px 5px 5px 5px;
	}
	.c1 {
		width:100px;
	}
	.c2 {
		width:200px;
	}
	a:link, a:visited {
		color:blue
	} 
	.container{  
		margin:0 auto;   
		width:500px;  
		text-align:center;  
	}  
 	p {
 		text-align:left;
 	}
</style>
</head>
<body>
    <div>
        <p>你好，管理员：<%= adminName%></p>
    </div>
    <div class="container">
        <h1>浏览用户名单</h1>  
        <%=user_msg%><br><br>
        <!--<div style="float:left">
            <a href="addStu.jsp">新增</a>
        </div>-->
        <br><br>
        <br><br>
    </div>
</body>
</html>

<%
	}
	else {
		System.out.println("未登录情况访问management.jsp");
		out.println("您无权访问此网站，将跳转回主页。");
		response.setHeader("refresh", "3;URL=index.html");
	}
%>