<%@ page language="java" import="java.util.*,java.sql.*" 
         contentType="text/html; charset=utf-8"
%>
<% request.setCharacterEncoding("utf-8"); %>
<%!
ArrayList< HashMap<String, Object> > query_all_comment(){
		String msg;
		ArrayList< HashMap<String, Object> > list = new ArrayList< HashMap<String, Object> >();
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String conStr = "jdbc:mysql://172.18.187.10:3306/WRTIS16337097"
			+ "?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8";
			Connection con =DriverManager.getConnection(conStr,"user", "123");
			Statement stmt =con.createStatement();
			ResultSet rs=stmt.executeQuery("select * from comment");
			while(rs.next()){
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("commentId", rs.getInt("commentId"));
				map.put("commentText", rs.getString("commentText"));
				map.put("userName", rs.getString("userName"));
				map.put("countryName", rs.getString("countryName"));
				map.put("commentFile", rs.getString("commentFile"));
				map.put("img1", rs.getString("commentImage1"));
				map.put("img2", rs.getString("commentImage2"));
				map.put("img3", rs.getString("commentImage3"));
				list.add(map);
			}
			rs.close();
			stmt.close();
			con.close();
			msg = "查询所有comment完毕";
			System.out.print(msg);
			return list;
		}
		catch(Exception e){
			msg = e.getMessage();
			System.out.println("连接失败\n");
			System.out.println(msg);
			return null;
		}
	}
%>
<%!
ArrayList< HashMap<String, Object> > query_comment_by_userName(String userName){
		String msg;
		ArrayList< HashMap<String, Object> > list = new ArrayList< HashMap<String, Object> >();
		try{
			Class.forName("com.mysql.jdbc.Driver");
			String conStr = "jdbc:mysql://172.18.187.10:3306/WRTIS16337097"
			+ "?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8";
			Connection con =DriverManager.getConnection(conStr,"user", "123");
			Statement stmt =con.createStatement();
			String sql = String.format("select * from comment where userName='%s'", userName);
			ResultSet rs=stmt.executeQuery(sql);
			while(rs.next()){
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("commentId", rs.getInt("commentId"));
				map.put("commentText", rs.getString("commentText"));
				map.put("userName", rs.getString("userName"));
				map.put("countryName", rs.getString("countryName"));
				map.put("commentFile", rs.getString("commentFile"));
				map.put("img1", rs.getString("commentImage1"));
				map.put("img2", rs.getString("commentImage2"));
				map.put("img3", rs.getString("commentImage3"));
				list.add(map);
			}
			rs.close();
			stmt.close();
			con.close();
			msg = "查询用户"+userName+"的comment完毕";
			System.out.println(msg);
			return list;
		}
		catch(Exception e){
			msg = e.getMessage();
			System.out.println("连接失败\n");
			System.out.println(msg);
			return null;
		}
	}
%>
<%
    String adminLoginStatus = (String)session.getAttribute("adminLogin");
    if("ok".equals(adminLoginStatus)) {
        System.out.println("登录情况下通过session访问management.jsp");
        String adminName = (String)session.getAttribute("admin");
        StringBuilder comment_msg = new StringBuilder();
		ArrayList< HashMap<String, Object> > list;
		if(request.getParameter("userName") == null)
			list = query_all_comment();
		else
			list = query_comment_by_userName(request.getParameter("userName"));
        comment_msg.append("<table><tr><th>id</th><th>内容</th><th>用户名</th><th>国家名</th>"+ 
        "<th>文件</th><th>img1</th><th>img2</th><th>img3</th><th>-</th></tr>");
        for(int i=0;i<list.size();i++){
            HashMap<String, Object> map = list.get(i);
            String commentFile = (String)map.get("commentFile");
            comment_msg.append(
                String.format( "<tr><td>%d</td><td>%s</td><td>%s</td><td>%s</td>"+
                    "<td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>",
                    map.get("commentId"),
                    map.get("commentText"),
                    map.get("userName"),
                    map.get("countryName"),
                    String.format("<a href='%s'>%s</a>",
                    	map.get("commentFile"),
                    	commentFile.substring(commentFile.indexOf("\\")+1)
                    ),
                    String.format("<img src='%s'>",
                    	map.get("img1")
                    ),
                    String.format("<img src='%s'>",
                    	map.get("img2")
                    ),
                    String.format("<img src='%s'>",
                     	map.get("img3")
                    ),
					String.format("<a href='deleteComment.jsp?commentId=%d'>删除</a>",
                        map.get("commentId")
                    )
                )
            );
        }
        comment_msg.append("</table>");
%>

<!DOCTYPE HTML>
<html>
<head>
<title>浏览评论</title>
<style>
	table{
		border-collapse: collapse;
		border: none;
	}
	td,th{
		border: solid grey 1px;            
		margin: 0 0 0 0;
		padding: 5px 5px 5px 5px;
	}
	a:link, a:visited {
		color:blue
	} 
	.container{  
		margin:0 auto;   
		text-align:center;  
	}  
 	p {
 		text-align:left;
 	}
 	img {
 		width: 40%;
 	}
</style>
</head>
<body>
    <div>
        <p>你好，管理员：<%= adminName%></p>
    </div>
    <div class="container">
        <h1>浏览评论</h1>  
        <%=comment_msg%><br><br>
        <br><br>
        <br><br>
    </div>
</body>
</html>

<%
	}
	else {
		System.out.println("未登录情况访问management.jsp");
		out.println("您无权访问此网站，将跳转回主页。");
		response.setHeader("refresh", "3;URL=index.html");
	}
%>