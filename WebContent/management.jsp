<%@ page language="java"  import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<% 
	request.setCharacterEncoding("utf-8"); //设定接受的字符串的编码为utf-8 

	String adminLoginStatus = (String)session.getAttribute("adminLogin");
	if("ok".equals(adminLoginStatus)) {
		System.out.println("登录情况下通过session访问management.jsp");
		String adminName = (String)session.getAttribute("admin");
%>
	
<!DOCTYPE html>
<html>
	<head>
		<title>Management</title>
	</head>
<body>
	<div>
		<p>你好，管理员：<%= adminName%></p>
	</div>
	<div>
		<a href="browseUsers.jsp" target="_blank">查看用户</a>
	</div>
	<div>
		<a href="browseComments.jsp" target="_blank">查看评论</a>
	</div>
</body>
</html>

<%
	}
	else {
		System.out.println("未登录情况访问management.jsp");
		out.println("您无权访问此网站，将跳转回主页。");
		response.setHeader("refresh", "3;URL=index.html");
	}
%>



