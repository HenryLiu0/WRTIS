<%@ page language="java" import="java.util.*,java.sql.*" 
         contentType="text/html; charset=utf-8"
%>
<% request.setCharacterEncoding("utf-8"); %>
<%!
    int delete_comment(int commentId){
        int rs=0;
        String msg = "";
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String conStr = "jdbc:mysql://172.18.187.10:3306/WRTIS16337097"
                          + "?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8";
            Connection con =DriverManager.getConnection(conStr,"user", "123");
            Statement stmt =con.createStatement();
            String sql = String.format("delete from comment where commentId='%d'", commentId);
            rs=stmt.executeUpdate(sql);
            if(rs > 0)
                msg = "Delete Success!";
            else
                msg = "Delete fail.";
            stmt.close();
            con.close();
            System.out.println(msg);
        }
        catch(Exception e){
            msg = e.getMessage();
            System.out.print("连接失败\n");
            System.out.print(msg);
        }
        return rs;
    }
%>
<%
String adminLoginStatus = (String)session.getAttribute("adminLogin");
if("ok".equals(adminLoginStatus)) {
    System.out.println("登录情况下通过session访问management.jsp");
    String adminName = (String)session.getAttribute("admin");
    int commentId = Integer.parseInt(request.getParameter("commentId"));
    int rs = delete_comment(commentId);
    String msg = "";
    if(rs > 0)
        msg = String.format("删除评论%d成功", commentId);
    else
        msg = String.format("删除评论%d失败", commentId);
%>
<%-- 有权限用户html代码 --%>
<!DOCTYPE html>
<html>
<head>
    <title>删除评论</title>
    <style>
        a:link,a:visited {color:blue}
        .container{  
            margin:0 auto; 
            width:500px;  
            text-align:center;  
        } 
    </style>
</head>
<body>
    <div>
		<p>你好，管理员：<%= adminName%></p>
	</div>
    <div class="container">
        <h1>删除评论</h1>
        <%=msg %>
        <br/><br/>
        <a href='browseComments.jsp'>返回</a>
    </div>
</body>
</html>
<%
	}
	else {
		System.out.println("未登录情况访问management.jsp");
		out.println("您无权访问此网站，将跳转回主页。");
		response.setHeader("refresh", "3;URL=index.html");
	}
%>