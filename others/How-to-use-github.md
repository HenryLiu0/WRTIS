# 我们怎么使用 github 进行协作？

## github 是什么？

github 是一个代码托管平台，用来保存代码的。

github 不只是一个代码托管平台，它的许多功能方便社会化共同软件开发，比如协作完成一个大项目。

本次是第一次完全使用 github 进行项目管理。

## 什么是仓库（Repositorie）？

github 上保存代码最大的单位，比如说一个项目保存进一个仓库。

### 什么是公有仓库（public Repositorie）？

你的公有仓库中的代码谁谁都能看到。

### 什么是私有仓库（private Repositorie）？

你的私有仓库只有你自己和项目协作者能看到。

## 什么是 issues？

> 觉得我说的不好？移步
> * [如何使用 Issue 管理软件项目？](http://www.ruanyifeng.com/blog/2017/08/issue.html)
> * [github issue是做什么的？](https://www.zhihu.com/question/22969033)

issues 长这样：

![](images/issues.png)

issue 可以用来发布**下一步要完成的工作（添加xxx特性，修复xxx bug等）**

每个 issue 可以：

* 添加**标签 label**表示issue的类型（bug、question等）和优先级（High、Medium、Low）
* 关联**人员 Assignee**指派人员处理issue

> 在公有仓库中，路人可以用 issue 给别人的项目提意见（报告bug，增加特性）。

当然，有什么想说的可以通过 issues 发布出来。

## 什么是 commit？

当你认为你代码适合被放在 github 上了，commit它，github 上的代码就是你commit后的样子。

### 什么时候 commit？

commit 有大学问，在你做出一个不大不小的更新后可以commit它，如果你对代码已经有信心了，可以commit它。代码每实现一点小更新，可以commit它。

注意：在我们的项目中，你只能对你自己fork的仓库commit。

## 什么是 fork？

每个人都有自己的仓库，托管在别人账户下的仓库可以复制一份到自己的，这就是fork。

### fork 有什么用？

fork 到自己账户的仓库可以进行编辑修改。

### 我们怎么用fork？

每个成员都要fork主仓库，然后在自己的仓库下修改增加代码。

注意：主仓库代码和你fork的代码不是同步的，主仓库代码也会变动。在我们的项目中，你需要时刻将自己的仓库与主仓库保持一致。

## 什么是 pull request？

当你认为你fork的仓库可以提交给主仓库后，用pull request。主仓库同意后，主仓库就会和你的仓库一样了。

### 我们怎么用 pull request？

我建议，添加了一项功能后就进行 pull request。我会时刻关注代码。

## 什么是 watch？

![](images/watch.png)

你关注着这个仓库，这个仓库的每一个举动都会通过邮件通知你

注意：每个成员都应该watch主仓库。

## 我们怎么做项目？

### 进度

* 通过主仓库的issue关注自己的任务、分工、进度。
* 有问题？有bug？想讨论？在主仓库上issue发出来，@你想@的人，说你想说的话。

### 代码

1. fork 主仓库。时刻保持自己的仓库和主仓库一致。
2. 在自己fork的仓库上进行代码更新（通过commit）。
3. 一天的工作结束后进行 pull request

### 沟通

* 试试尽量通过主仓库的issue沟通，方便后期做文档

## 如何与我沟通？

还有什么想知道的？还有什么不会的？

试试在主仓库上发issue并@我。

## 后话

> 雄关漫道真如铁，而今迈步从头越——毛泽东