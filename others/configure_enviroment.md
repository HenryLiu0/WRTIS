# configure enviroment

要求环境

1. jdk1.8.0_102
2. Tomcat 8.5
3. eclipse-jee-oxygen-3a-win32-x86_64
4. mysql(未配置)

## jdk1.8.0_102

![jdk](images/jdk.png)

配置环境变量例如：

![jdk_home](images/jdk_home.png)

添加path例如：

![jdk_path](images/jdk_path.png)

## Tomcat 8.5

![tomcat](images/tomcat.png)

安装后启动：

双击 `tomcat8.5/bin/startup.bat`

测试是否成功：

浏览器登录 `localhost:8080` 是否有一只猫：

![cat](images/root.png)

## eclipse-jee-oxygen-3a-win32-x86_64

[下载地址，使用清华TUNA镜像下载更快](https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/oxygen/3a/eclipse-jee-oxygen-3a-win32-x86_64.zip)

字符编码：

![encoding](images/encoding.png)

创建javaweb工程：

![javaweb](images/javaweb.png)

选择servlet版本2.5：

![servlet_version](images/sevlet_version.png)

